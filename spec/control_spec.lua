local ARMOR_COUNT = 1
_G.script = {
	on_event = function(_, _) end
}
_G.defines = {
	events = {
		on_tick = {
			name = "on_tick",
			tick = 1
		}
	},
	inventory = {
		player_armor = "something"
	}
}

_G.game = {
	connected_players = {
		{
			character = true,
			get_inventory = function(_)
				return {
					get_item_count = function(_)
						return ARMOR_COUNT
					end
				}
			end,
			surface = {
				create_entity = function(_) end
			},
			position = "a"
		}
	}
}
local control = require("control")

describe("Spread Fire -", function()
	local snapshot

	before_each(function()
		snapshot = assert:snapshot()
	end)

	after_each(function()
		snapshot:revert()
	end)

	it("should spread fire when player has armor and the timing is good", function()
		local event = {
			tick = 60
		}
		local spy_create_entity = spy.on(_G.game.connected_players[1].surface, "create_entity")
		-- When
		control.spread_fire(event)
		-- Then
		assert.spy(spy_create_entity).was.called(1)
	end)
	it("should not spread fire when player has no armor", function()
		-- Given
		ARMOR_COUNT = 0
		local event = {
			tick = 60
		}
		local spy_create_entity = spy.on(_G.game.connected_players[1].surface, "create_entity")
		-- When
		control.spread_fire(event)
		-- Then
		assert.spy(spy_create_entity).was.called(0)
	end)
	it("should not spread fire when it's not the right tick", function()
		-- Given
		local event = {
			tick = 42
		}
		local spy_create_entity = spy.on(_G.game.connected_players[1].surface, "create_entity")
		-- When
		control.spread_fire(event)
		-- Then
		assert.spy(spy_create_entity).was.called(0)
	end)
end)
